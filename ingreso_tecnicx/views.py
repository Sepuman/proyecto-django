from datetime import date

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

# Create your views here.
from .models import *


@login_required(login_url='')
def vista_ingresos(request):
    ingreso = formulario_ingreso()
    return render(request, 'ingresar/ingresos.html', context={"nombrerandom": ingreso})  #este "nombrerandom" hay que indexarlo al archivo html


@login_required(login_url='')
def get_info_ingresos(request):
    if request.method == 'POST':
        form = formulario_ingreso(request.POST)
        print(form.is_valid())
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/ingresos')
    else:
        return redirect('')


## cc_myself = forms.BooleanField(required=False)