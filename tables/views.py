from django.contrib.auth.decorators import login_required
from django.shortcuts import render

# Create your views here.
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin
from django_tables2.export import ExportMixin

from tables.filters import FHFilter, DotFilter
from tables.models import flota_FueraHorario, dotacion, geovictoriav2
from tables.tables import FHTable, DotacionTable, GeovictoriaTable


class FHListView(ExportMixin, SingleTableMixin, FilterView):
    model = flota_FueraHorario
    table_class = FHTable
    template_name = 'flota/fuera_horario.html'
    filterset_class = FHFilter
    export_formats = ("latex", "csv", "xlsx")

    def get_table_kwargs(self):
        return {"template_name": "django_tables2/bootstrap.html"}


class DotacionListView(ExportMixin, SingleTableMixin, FilterView):
    model = dotacion
    table_class = DotacionTable
    template_name = 'rrhh/rrhh_tabla.html'
    filterset_class = DotFilter
    export_formats = ("latex", "csv", "xlsx")

    def get_table_kwargs(self):
        return {"template_name": "django_tables2/bootstrap.html"}


class GeovictoriaListView(ExportMixin, SingleTableMixin, FilterView):
    model = geovictoriav2
    table_class = GeovictoriaTable
    template_name = 'rrhh/rrhh_tabla.html'
    #filterset_class = FHFilter
    export_formats = ("latex", "csv", "xlsx")

    def get_table_kwargs(self):
        return {"template_name": "django_tables2/bootstrap.html"}


@login_required(login_url='')
def pag1(request):
    context = {'nbar': 'flota'}
    return render(request, 'flota/fuera_horario.html', context=context)


@login_required(login_url='')
def flota_tablero(request):
    return render(request, 'flota/tablero.html')


@login_required(login_url='')
def ssoma_tablero(request):
    context = {'nbar': 'ssoma'}
    return render(request, 'ssoma/tablero.html', context=context)
