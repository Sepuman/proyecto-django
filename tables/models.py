from compositefk.fields import CompositeForeignKey
from django.db import models

# Create your models here.

class db_flota(models.Model):
    jop = models.CharField(max_length=200)
    sup = models.CharField(max_length=200)
    rut = models.CharField(max_length=200)


class dotacion(models.Model):
    Rut = models.CharField(max_length=200, primary_key=True)
    Sigla = models.CharField(max_length=200)
    Nombre = models.CharField(max_length=200)
    Supervisor = models.CharField(max_length=200)
    Comuna = models.CharField(max_length=200)
    Zona = models.CharField(max_length=200)
    Cargo = models.CharField(max_length=200)
    Tipo = models.CharField(max_length=200)
    Tipo_ctto = models.CharField(max_length=200)
    Proyecto = models.CharField(max_length=200)
    Con_o_sin_experiencia = models.CharField(max_length=200)
    JOP = models.CharField(max_length=200)


class flota_FueraHorario(models.Model):
    Patente = models.CharField(max_length=200)
    proveedor = models.CharField(max_length=200)
    inicio = models.DateTimeField()
    fin = models.DateTimeField()
    eventos = models.DecimalField(max_digits=10, decimal_places=2)
    kms = models.DecimalField(max_digits=10, decimal_places=2)
    empleado = models.CharField(max_length=200)
    #matching = CompositeForeignKey(db_flota, on_delete=models.CASCADE, related_name='dd', to_fields={
     #   'jop': 'Patente',
      #  'sup': 'proveedor'
    #}
                     #              )


class geovictoriav2(models.Model):
    Apellidos = models.CharField(max_length=200)
    Nombre = models.CharField(max_length=200)
    Rut = models.ForeignKey(dotacion, on_delete=models.CASCADE)
    Grupo = models.CharField(max_length=200)
    Fecha = models.DateTimeField()
    Permiso = models.CharField(max_length=200)
    Turno = models.CharField(max_length=200)
    Entro = models.TimeField(auto_now=False, auto_now_add=False, default=None, blank=True, null=True)
    Salio = models.TimeField(auto_now=False, auto_now_add=False, default=None, blank=True, null=True)
    Cargo = models.CharField(max_length=200)



