import django_tables2
from django_tables2 import tables

from tables.models import flota_FueraHorario, dotacion, geovictoriav2


class FHTable(tables.Table):
    jop = django_tables2.Column(accessor='matching.rut')

    class Meta:
        model = flota_FueraHorario
        exclude = ('matching',)
        template_name = "django_tables2/bootstrap.html"


class DotacionTable(tables.Table):
    class Meta:
        model = dotacion
        template_name = "django_tables2/bootstrap.html"


class GeovictoriaTable(DotacionTable, tables.Table):
    class Meta:
        model = geovictoriav2
        template_name = "django_tables2/bootstrap.html"
class DotacionTable(tables.Table):
    class Meta:
        model = dotacion
        template_name = "django_tables2/bootstrap.html"


class GeovictoriaTable(tables.Table):
    class Meta:
        model = geovictoriav2
        template_name = "django_tables2/bootstrap.html"
