from django.contrib.auth.decorators import login_required
from django.urls import path

from tables import views
from tables.views import FHListView, DotacionListView, GeovictoriaListView

app_name = 'tables'
urlpatterns = [
    path('flota/', login_required(FHListView.as_view()), name='flota'),
    path('flota_tablero/', views.flota_tablero, name='flota_tablero'),
    path('ssoma_tablero/', views.ssoma_tablero, name='ssoma_tablero'),
    path('rrhh_tabla/', login_required(DotacionListView.as_view()), name='tabla_dotacion'),
    path('rrhh_tabla_geo/', login_required(GeovictoriaListView.as_view()), name='tabla_geo'),
]