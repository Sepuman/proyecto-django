from django_filters import FilterSet, DateFromToRangeFilter
from django_filters.widgets import RangeWidget
from .models import flota_FueraHorario, dotacion


class FHFilter(FilterSet):
    inicio = DateFromToRangeFilter(widget=RangeWidget(attrs={'placeholder': 'YYYY/MM/DD', 'id': 'datepicker', 'autocomplete': 'off'}))

    class Meta:
        model = flota_FueraHorario
        fields = {'Patente': ['exact'], 'kms': ['gt'], 'inicio': []}


class DotFilter(FilterSet):
    class Meta:
        model = dotacion
        fields = {'Rut': ['exact'],}