from django.urls import path

from . import views

app_name = 'welcomes'
urlpatterns = [
    path('', views.welcome, name='welcome'),
    path('set_email/', views.set_email, name='set_email'),
    path('change_email/', views.change_email, name='change_email'),
    path('set_pass/', views.set_pass, name='set_pass'),
    path('change_pass/', views.change_pass, name='change_pass'),

]
