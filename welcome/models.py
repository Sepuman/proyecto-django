from django.contrib.auth.models import User
import pandas as pd
# Create your models here.
from django.db import models


class CreateUsers:
    def __init__(self):
        self.file = pd.read_csv('Actualización Salfa - INDIRECTO.csv', usecols=['RUT', 'NOMBRE', 'CORREO'])

    def p(self):
        print(self.file.head())

    # it can be done with apply
    def creation(self):
        ruts = self.file['RUT'].unique()
        for rut in ruts:
            serie_rut = self.file.loc[self.file['RUT'].isin([rut])]
            email = str(serie_rut['CORREO'].to_numpy()[0])
            nombre_corto = str(serie_rut['NOMBRE'].to_numpy()[0])
            #nombre_corto = str(serie_rut['NOMBRE CORTO'].to_numpy()[0])
            user = User.objects.create_user(rut, email, rut.split('-')[0][-4:])
            user.first_name = nombre_corto.split()[0]
            user.last_name = nombre_corto.split()[1]
            user.save()
