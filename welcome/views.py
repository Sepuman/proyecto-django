from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse


@login_required(login_url='')
def welcome(request):
    context = {'nbar': 'home'}
    return render(request, 'welcome/welcome.html', context=context)


@login_required(login_url='')
def set_email(request):
    return render(request, 'welcome/set_email.html')


@login_required(login_url='')
def change_email(request):
    email = request.POST['email']
    u = request.user
    u.email = email
    u.save()

    return HttpResponseRedirect(reverse('welcome:welcome'))


@login_required(login_url='')
def set_pass(request):
    form = PasswordChangeForm(request.user)
    context = {'form': form}
    return render(request, 'welcome/set_pass.html', context=context)


@login_required(login_url='')
def change_pass(request):
    pass1 = request.POST['new_password1']
    pass2 = request.POST['new_password2']
    valid = pass1 == pass2
    if valid:
        u = request.user
        u.set_password(pass1)
        u.save()
    context = {'valid': valid}
    return render(request, 'welcome/valid_pass.html', context=context)









